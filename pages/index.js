import Head from 'next/head'
import Header from '../components/Header'

export default function Home() {
  return (
    <div>
      <Head>
        <title>rooter-editor</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

    <Header />
    </div>
  )
}
